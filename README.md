# PT. Digdaya Olah Teknologi Indonesia

# Intalasi
1. clone repo git@gitlab.com:abdulhanan13/dot_api.git
2. composer install
3. set .env `RAJA_ONGKIR_URL, RAJA_ONGKIR_KEY, SWAP_API` (ada di .env.example)
4. `php artisan vendor:publish --provider="PHPOpenSourceSaver\JWTAuth\Providers\LaravelServiceProvider"`
5. `php artisan jwt:secret`
6. `php artisan migrate` 
7. `php artisan db:seed --class=UserSeeder` untuk initialize data user
8. ` php artisan fetchData:list city` untuk fetch data city dari api raja ongkir
9. `php artisan fetchData:list province` untuk fetch data province dari api raja ongkir

## Auth 

untuk menggunakan api ini agar dapat berjalan dengan baik diharuskan login terlerbih dahulu dengan menggunakan email dan password sbb:
`email : hanan.dev@gmail.com`
`password: password`
untuk endpoint yang digunakan 
`{{BASE_URL}}/login`
`headers: {
	"Accept": "application/json"
	}
	`
	sample response
	`{
		"status": "success",
			"user": {
				"id": 1,
				"name": "Hanan",
				"email": "hanan.dev@gmail.com",
				"email_verified_at": null,
				"created_at": null,
				"updated_at": null
			},
	"authorization": {
"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjEwMDAyL2xvZ2luIiwiaWF0IjoxNjY3MjMyODY1LCJleHAiOjE2NjcyMzY0NjUsIm5iZiI6MTY2NzIzMjg2NSwianRpIjoiNndrZWxRSkJtVnhOUEkwdCIsInN1YiI6IjEiLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.44lTVi4R9eqyZWewHmzmbgP2hFOXv76yi-efzNvgtsQ",
"type": "bearer"
	}
} `

copy value pada token untuk kemudian digunakan sebagai authorization 

## Penggunaan API

**Api Cities** 
`{{URL}}/search/cities?id=1`
`headers: {
	"Authorization":  Bearer {token}
	"Accept": "application/json"
}`

**Api Provinces** 
`{{URL}}/search/provinces?id=1`
`headers: {
	"Authorization":  Bearer {token}
	"Accept": "application/json"
}`

## Unit Test

untuk menjalankan unit test cukup degan menjalankan command `php artisan test`

